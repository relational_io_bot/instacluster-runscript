#!/usr/bin/env python
### Script provided by DataStax.

import exceptions
import glob
import os
import re
import shlex
import subprocess
import sys
import time
import traceback
import urllib2

from optparse import OptionParser

import logger
import conf

# Full path must be used since this script will execute at
# startup as no root user
import utils

instance_data = {}
config_data = {}
config_data['conf_path'] = os.path.expanduser("/etc/cassandra/")
config_data['opsc_conf_path'] = os.path.expanduser("/etc/opscenter/")
options = False

tokens = {}

def setup_repos():
    # Add repos
    logger.pipe('echo "deb http://debian.datastax.com/community stable main"', 'sudo tee -a /etc/apt/sources.list.d/datastax.sources.list')

    # Add repokeys
    logger.pipe('curl -s http://installer.datastax.com/downloads/ubuntuarchive.repo_key', 'sudo apt-key add -')
    logger.pipe('curl -s http://opscenter.datastax.com/debian/repo_key', 'sudo apt-key add -')
    logger.pipe('curl -s http://debian.datastax.com/debian/repo_key', 'sudo apt-key add -')

    # Perform the install
    logger.exe('sudo apt-get update')
    while True:
        output = logger.exe('sudo apt-get update')
        break
#        if not output[1] and not 'err' in output[0].lower() and not 'failed' in output[0].lower():
#            break
    time.sleep(5)

def clean_installation():
    logger.info('Performing deployment install...')
    if conf.get_config("AMI", "Type") == "Community":
        logger.exe('sudo apt-get install -y cassandra=1.1.12')
        logger.exe('sudo apt-get install -y python-cql dsc1.1=1.1.12-1 ')
        conf.set_config('AMI', 'package', 'dsc1.1')
        # logger.exe('sudo apt-get install -y dsc-demos')
        logger.exe('sudo service cassandra stop')

    # Remove the presaved information from startup
    logger.exe('sudo rm -rf /var/lib/cassandra')
    logger.exe('sudo rm -rf /var/log/cassandra')
    logger.exe('sudo mkdir -p /var/lib/cassandra')
    logger.exe('sudo mkdir -p /var/log/cassandra')
    logger.exe('sudo chown -R tomcat7:tomcat7 /var/lib/cassandra')
    logger.exe('sudo chown -R tomcat7:tomcat7 /var/log/cassandra')
    logger.exe('sudo chown -R tomcat7:tomcat7 /etc/cassandra')

def edit_cassandra_startup():
    with open('/etc/init.d/cassandra', 'r') as f:
        config = f.read()

    # Changed Daemon
    config = config.replace('org.apache.cassandra.thrift.CassandraDaemon', 'com.netflix.priam.cassandra.NFThinCassandraDaemon')
    config = config.replace('printf "$cp:$CONFDIR:/usr/share/java/commons-daemon.jar"', 'printf "$CONFDIR:$cp:/usr/share/java/commons-daemon.jar"')
    config = config.replace('-user cassandra','-user tomcat7')

    with open('/etc/init.d/cassandra', 'w') as f:
        f.write(config)

    logger.info('cassandra init script configured.')


def priam_installation():
    logger.exe('sudo service cassandra stop')
    logger.exe('wget -q https://s3.amazonaws.com/instaclustr-resources/java/priam-latest.jar')
    logger.exe('wget -q https://s3.amazonaws.com/instaclustr-resources/java/priam-web-latest.war')
    logger.exe('sudo cp priam-latest.jar /usr/share/cassandra/lib/priam.jar')
    logger.exe('sudo cp priam-web-latest.war /var/lib/tomcat7/webapps/Priam.war')
    logger.exe('sudo service tomcat7 restart')
    logger.exe('sudo service cassandra restart')


def checkpoint_info():
#    if not options.raidonly:
#        logger.info("OpsCenter: {0}".format(config_data['opscenterseed']))
#        conf.set_config("AMI", "LeadingSeed", config_data['opscenterseed'])
    conf.set_config("AMI", "CurrentStatus", "Installation complete")




def construct_env():
    with open(os.path.join(config_data['conf_path'], 'cassandra-env.sh'), 'r') as f:
        cassandra_env = f.read()

    # Clear commented line
    cassandra_env = cassandra_env.replace('# JVM_OPTS="$JVM_OPTS -Djava.rmi.server.hostname=<public name>"', 'JVM_OPTS="$JVM_OPTS -Djava.rmi.server.hostname=%s"' % instance_data['publichostname'])

    with open(os.path.join(config_data['conf_path'], 'cassandra-env.sh'), 'w') as f:
        f.write(cassandra_env)

    logger.info('cassandra-env.sh configured.')

def mount_raid(devices):
    # Make sure the devices are umounted, then run fdisk on each device
    logger.info('Clear "invalid flag 0x0000 of partition table 4" by issuing a write, then running fdisk on each device...')
    formatCommands = "echo 'n\np\n1\n\n\nt\nfd\nw'"
    for device in devices:
        logger.info('Confirming devices are not mounted:')
        logger.exe('sudo umount {0}'.format(device), False)
        logger.pipe("echo 'w'", 'sudo fdisk -c -u {0}'.format(device))
        logger.pipe(formatCommands, 'sudo fdisk -c -u {0}'.format(device))

    # Create a list of partitions to RAID
    logger.exe('sudo fdisk -l')
    partitions = glob.glob("/dev/sd*[0-9]")
    if not partitions:
        partitions = glob.glob("/dev/xvd*[0-9]")
        partitions.remove('/dev/xvda1')
    else:
        partitions.remove('/dev/sda1')
    partitions.sort()

    logger.info('Partitions about to be added to RAID0 set: {0}'.format(partitions))

    # Make sure the partitions are umounted and create a list string
    partion_list = ''
    for partition in partitions:
        logger.info('Confirming partitions are not mounted:')
        logger.exe('sudo umount ' + partition, False)
    partion_list = ' '.join(partitions).strip()

    logger.info('Creating the RAID0 set:')
    time.sleep(3) # was at 10

    conf.set_config("AMI", "CurrentStatus", "Raid creation")

    # Continuously create the Raid device, in case there are errors
    raid_created = False
    retryCount = 30
    counter = 0
    while not raid_created and counter < retryCount:
        logger.exe('sudo mdadm --create /dev/md0 --chunk=256 --level=0 --raid-devices={0} {1}'.format(len(devices), partion_list), expectError=True)
        raid_created = True

        logger.pipe('echo DEVICE {0}'.format(partion_list), 'sudo tee /etc/mdadm/mdadm.conf')
        time.sleep(5)
        logger.pipe('mdadm --detail --scan', 'sudo tee -a /etc/mdadm/mdadm.conf')
        time.sleep(10)
        logger.exe('blockdev --setra 512 /dev/md0')

        logger.info('Formatting the RAID0 set:')
        time.sleep(10)
        raidError = logger.exe('sudo mkfs.xfs -f /dev/md0', expectError=True)[1]

        if raidError:
            logger.exe('sudo mdadm --stop /dev/md_d0', expectError=True)
            logger.exe('sudo mdadm --zero-superblock /dev/sdb1', expectError=True)
            raid_created = False
            counter += 1

    if counter > retryCount and not raid_created:
        utils.halt_me("Could not create RAID partition")

    # Configure fstab and mount the new RAID0 device
    mnt_point = '/raid0'
    logger.pipe("echo '/dev/md0\t{0}\txfs\tdefaults,nobootwait,noatime\t0\t0'".format(mnt_point), 'sudo tee -a /etc/fstab')
    logger.exe('sudo mkdir {0}'.format(mnt_point))
    logger.exe('sudo mount -a')
    logger.exe('sudo mkdir -p {0}'.format(os.path.join(mnt_point, 'cassandra')))
    logger.exe('sudo chown -R tomcat7:tomcat7 {0}'.format(os.path.join(mnt_point, 'cassandra')))

    logger.info('Showing RAID0 details:')
    logger.exe('cat /proc/mdstat')
    logger.exe('echo "15000" > /proc/sys/dev/raid/speed_limit_min')
    logger.exe('sudo mdadm --detail /dev/md0')
    return mnt_point

def format_xfs(devices):
    # Make sure the device is umounted, then run fdisk on the device
    logger.info('Clear "invalid flag 0x0000 of partition table 4" by issuing a write, then running fdisk on the device...')
    formatCommands = "echo 'd\nn\np\n1\n\n\nt\n83\nw'"
    logger.exe('sudo umount {0}'.format(devices[0]))
    logger.pipe("echo 'w'", 'sudo fdisk -c -u {0}'.format(devices[0]))
    logger.pipe(formatCommands, 'sudo fdisk -c -u {0}'.format(devices[0]))

    # Create a list of partitions to RAID
    logger.exe('sudo fdisk -l')
    partitions = glob.glob("/dev/sd*[0-9]")
    if not partitions:
        partitions = glob.glob("/dev/xvd*[0-9]")
        partitions.remove('/dev/xvda1')
    else:
        partitions.remove('/dev/sda1')
    partitions.sort()

    logger.info('Formatting the new partition:')
    logger.exe('sudo mkfs.xfs -f {0}'.format(partitions[0]))

    # Configure fstab and mount the new formatted device
    mnt_point = '/raid0'
    logger.pipe("echo '{0}\t{1}\txfs\tdefaults,nobootwait,noatime\t0\t0'".format(partitions[0], mnt_point), 'sudo tee -a /etc/fstab')
    logger.exe('sudo mkdir {0}'.format(mnt_point), False)
    logger.exe('sudo mount -a')
    logger.exe('sudo mkdir -p {0}'.format(os.path.join(mnt_point, 'cassandra')))
    logger.exe('sudo chown -R tomcat7:tomcat7 {0}'.format(os.path.join(mnt_point, 'cassandra')))
    return mnt_point

def prepare_for_raid():
    #Give the OS some time to finish playing with the partition table
    time.sleep(10)

    # Only create raid0 once. Mount all times in init.d script. A failsafe against deleting this file.
    if conf.get_config("AMI", "RAIDAttempted"):
        return

    conf.set_config("AMI", "CurrentStatus", "Raiding started")

    # Remove EC2 default /mnt from fstab
    fstab = ''
    file_to_open = '/etc/fstab'
    logger.exe('sudo chmod 777 {0}'.format(file_to_open))
    with open(file_to_open, 'r') as f:
        for line in f:
            if not "/mnt" in line:
                fstab += line
    with open(file_to_open, 'w') as f:
        f.write(fstab)
    logger.exe('sudo chmod 644 {0}'.format(file_to_open))

    # Create a list of devices
    devices = glob.glob("/dev/sd*")
    if not devices:
        devices = glob.glob("/dev/xvd*")
        devices.remove('/dev/xvda1')
    else:
        devices.remove('/dev/sda1')



    devices.sort()
    logger.info('Unformatted devices: {0}'.format(devices))

    # Check if there are enough drives to start a RAID set
    if len(devices) > 1:
        time.sleep(3) # was at 20
        mnt_point = mount_raid(devices)

    # Not enough drives to RAID together.
    else:
        mnt_point = format_xfs(devices)

    if not options.raidonly:
        # Change cassandra.yaml to point to the new data directories
        with open(os.path.join(config_data['conf_path'], 'cassandra.yaml'), 'r') as f:
            yaml = f.read()

        yaml = yaml.replace('/var/lib/cassandra/data', os.path.join(mnt_point, 'cassandra', 'data'))
        yaml = yaml.replace('/var/lib/cassandra/saved_caches', os.path.join(mnt_point, 'cassandra', 'saved_caches'))
        yaml = yaml.replace('/var/lib/cassandra/commitlog', os.path.join(mnt_point, 'cassandra', 'commitlog'))

        with open(os.path.join(config_data['conf_path'], 'cassandra.yaml'), 'w') as f:
            f.write(yaml)

    # Never create raid array again
    conf.set_config("AMI", "RAIDAttempted", True)

    logger.info("Mounted Raid.\n")
    conf.set_config("AMI", "MountDirectory", mnt_point)
    conf.set_config("AMI", "CurrentStatus", "Raiding complete")

def sync_clocks():
    # Confirm that NTP is installed
    logger.exe('sudo apt-get -y install ntp')

    with open('/etc/ntp.conf', 'r') as f:
            ntp_conf = f.read()

    # Create a list of ntp server pools
    server_list = ""
    for i in range(0, 4):
        server_list += "server {0}.north-america.pool.ntp.org\n".format(i)

    # Overwrite the single ubuntu ntp server with the server pools
    ntp_conf = ntp_conf.replace('server ntp.ubuntu.com', server_list)

    with open('/etc/ntp.conf', 'w') as f:
        f.write(ntp_conf)

    # Restart the service
    logger.exe('sudo service ntp restart')

def additional_pre_configurations():
    logger.exe('gpg --keyserver pgp.mit.edu --recv-keys 40976EAF437D05B5', expectError=True)
    logger.pipe('gpg --export --armor 40976EAF437D05B5', 'sudo apt-key add -')
    pass

def additional_post_configurations():
    logger.exe('sudo apt-get install s3cmd')
    logger.exe('sudo pip install boto --upgrade')



def reset_ssl():
    for path in glob.glob('/etc/ssl/certs/*.bak'):
        os.rename(path,path[:-3] + "pem")

def run():
    reset_ssl()
    # Remove script files
    additional_pre_configurations()
    global options
    global instance_data
    instance_data = utils.get_ec2_data()
    options = utils.parse_ec2_userdata(instance_data)

    if not options.raidonly:
        utils.use_ec2_userdata(options)
        logger.info(options)
        utils.send_message_to_instaclustr("Mounting RAID partitions", progress="RAID_SETUP")
        setup_repos()
        clean_installation()
        construct_env()
        edit_cassandra_startup()

    checkpoint_info()
    #Allow the OS some time to get ready before RAID
    time.sleep(10)
    prepare_for_raid()

    logger.exe('sudo rm ds2_configure.py')
    logger.info('Deleting ds2_configure.py now. This AMI will never change any configs after this first run.')

    if not options.raidonly:
        sync_clocks()
        priam_installation()
        additional_post_configurations()

    logger.info("ds2_configure.py completed!\n")
    conf.set_config("AMI", "CurrentStatus", "Complete!")
