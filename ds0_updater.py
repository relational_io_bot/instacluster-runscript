#!/usr/bin/env python
### Script provided by DataStax.

import logger
import conf

# Update the AMI codebase if it's its first booot
import utils

options = utils.parse_ec2_userdata(utils.get_ec2_data())

if not conf.get_config("AMI", "CompletedFirstBoot"):

    logger.exe("""sudo su ubuntu -c 'cd /home/ubuntu/datastax_ami; git checkout 2.4'""",withShell=True)
    logger.exe("""sudo su ubuntu -c 'cd /home/ubuntu/datastax_ami; git pull'""",withShell=True)
    logger.exe("""sudo su ubuntu -c 'cd /home/ubuntu/datastax_ami; git pull origin :refs/tags/%s'""" %options.bootscripttag, withShell=True)
    logger.exe("""sudo su ubuntu -c 'cd /home/ubuntu/datastax_ami; git checkout %s' """ % options.bootscripttag, expectError=True)

# Start AMI start code
try:
    import ds1_launcher
    ds1_launcher.run()
except:
    logger.exception('ds0_updater.py')
