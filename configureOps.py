import sys
import boto
from boto.ec2.instance import Instance
from boto.s3.key import Key
from utils import curl_instance_data
import logger
import conf
import os
import re
import time
import urllib2
import utils

config_data = {}
instance_data = {}
config_data['conf_path'] = os.path.expanduser("/etc/cassandra/")
config_data['opsc_conf_path'] = os.path.expanduser("/etc/opscenter/")
config_data['opscenterseed'] = ""
options = False


def construct_opscenter_conf():
    try:
        with open(os.path.join(config_data['opsc_conf_path'], 'opscenterd.conf'), 'r') as f:
            opsc_conf = f.read()

        # Configure OpsCenter
        opsc_conf = opsc_conf.replace('port = 8080', 'port = 8888')
        opsc_conf = opsc_conf.replace('127.0.0.1', '0.0.0.0')

        conf.set_config("OpsCenter", "port", 8888)

        with open(os.path.join(config_data['opsc_conf_path'], 'opscenterd.conf'), 'w') as f:
            f.write(opsc_conf)

        logger.info('opscenterd.conf configured.')
    except:
        logger.info('opscenterd.conf not configured since conf was unable to be located.')


def construct_opscenter_cluster_conf():
    clustername = conf.get_config("Cluster", "Name")
    clustername = clustername if clustername else 'Could_not_detect_cluster_name'
    cluster_conf = '%s.conf' % clustername

    try:
        opsc_cluster_path = os.path.join(config_data['opsc_conf_path'], 'clusters')
        if not os.path.exists(opsc_cluster_path):
            os.mkdir(opsc_cluster_path)

        opsc_cluster_conf = """[jmx]
username =
password =
port = 7199

[cassandra]
username =
seed_hosts = {0}
api_port = 9160
password =
"""

        retry_count = 0
        retry_max = 30
        # Configure OpsCenter Cluster
        while len(config_data['opscenterseed']) < 1 and retry_count < retry_max:
            req = curl_instance_data("http://localhost:8080/Priam/REST/v1/cassconfig/get_seeds")
            try:
                config_data['opscenterseed'] = urllib2.urlopen(req).read()
            except:
                logger.info(sys.exc_info()[0])
                logger.info('Priam was not ready with request... waiting')
            time.sleep(4)
            retry_count += 1

        opsc_cluster_conf = opsc_cluster_conf.format(config_data['opscenterseed'])

        with open(os.path.join(opsc_cluster_path, cluster_conf), 'w') as f:
            f.write(opsc_cluster_conf)

        logger.info('opscenter/%s configured.' % cluster_conf)
    except:
        logger.info('opscenter/%s not configured since opscenter was unable to be located.' % cluster_conf)


def install_opscenter(s3bucket):
    logger.exe('sudo apt-get -y install opscenter-free=2.1.3-1')
    conn = boto.connect_s3(host=s3bucket['endpoint'])
    bucket = conn.get_bucket(s3bucket['bucketName'])
    key = Key(bucket)
    finished = False
    retryCount = 30
    counter = 0
    while not finished and counter < retryCount:
        try:
            f = open('/usr/share/opscenter/agent.tar.gz', 'r')
            finished = True
        except IOError, e:
            if e.errno == 2:
                counter += 1
                time.sleep(4)
                continue
            else:
                raise e

    key.key = '%s/agent.tar.gz' % config_data['clustername']
    key.set_contents_from_file(f, replace=True)
    logger.exe('sudo service opscenterd stop')


def start_opscenter():
    logger.exe('sudo service opscenterd start')


def download_callback(complete, total):
    if complete >= total:
        logger.info("complete %s : total %s" % (complete, total))

def install_agent(s3bucket, opsCenter):
    logger.info("Starting agent install")
    try:
        logger.exe('sudo apt-get -y remove --purge opscenter-agent')
    except Exception, e:
        print e

    conn = boto.connect_s3(host=s3bucket['endpoint'])
    bucket = conn.get_bucket(s3bucket['bucketName'])
    key = Key(bucket)
    key.key = '%s/agent.tar.gz' % config_data['clustername']
    finished = False
    retryCount = 60
    counter = 0
    while not finished and counter < retryCount:
        try:
            key.get_contents_to_filename('/home/ubuntu/agent.tar.gz', cb=download_callback)
            finished = True
        except boto.exception.S3ResponseError, e:
            counter += 1
            time.sleep(10)
            continue

    time.sleep(5)
    logger.exe('cd /home/ubuntu;sudo tar -xzf /home/ubuntu/agent.tar.gz', withShell=True)
    logger.exe('sudo /home/ubuntu/agent/bin/install_agent.sh opscenter-agent.deb %s' % opsCenter.private_dns_name, withShell=True)
    #try again in case file handle closed?


def main():
    logger.info(sys.path)
    logger.info(boto.__version__)

    req = curl_instance_data('http://169.254.169.254/latest/meta-data/instance-id')
    config_data['instanceid'] = urllib2.urlopen(req).read()

    req = curl_instance_data('http://169.254.169.254/latest/meta-data/placement/availability-zone')
    config_data['zone'] = urllib2.urlopen(req).read()
    config_data['region'] = config_data['zone'][:-1]

    config_data['clustername'] = conf.get_config("Cluster", "Name")
    finished = False
    retryCount = 30
    counter = 0
    while not finished and counter < retryCount:
        try:
            conn = boto.ec2.connect_to_region(config_data['region'])
            finished = True
        except boto.exception.NoAuthHandlerFound, e:
            counter += 1
            time.sleep(4)
            print e

    opsCenterInstances = []
    isOpscenter = False

    while not len(opsCenterInstances):
        conn = boto.ec2.connect_to_region(config_data['region'])
        filters = {'tag:ClusterName': config_data['clustername'], 'tag:Opscenter': 'true'}
        reservations = conn.get_all_instances(filters=filters)
        opsCenterInstances = [i for r in reservations for i in r.instances]

        opsCenter = Instance()

        for i in opsCenterInstances:
            opsCenter = i
            if i.id == config_data['instanceid']:
                isOpscenter = True
                break


    conn = boto.connect_sdb()
    dom = conn.get_domain('PriamProperties')
    s3bucket = {}
    s3bucket['bucketName'] = dom.get_item('%s.priam.s3.bucket' % config_data['clustername'])['value']
    if config_data['region'] == 'us-east-1':
        s3bucket['endpoint'] = 's3.amazonaws.com'
    else:
        s3bucket['endpoint'] = 's3-%s.amazonaws.com' % config_data['region']

    logger.info("connecting to %s" % s3bucket['bucketName'])
    logger.info("s3 endpoint chosen: %s" % s3bucket['endpoint'])

    #give priam some time to talk to the cluster and sort its stuff out
    if isOpscenter:
        install_opscenter(s3bucket)
        construct_opscenter_conf()
        construct_opscenter_cluster_conf()
        start_opscenter()

    utils.send_message_to_instaclustr("Install agents", progress="INSTALL_AGENT")
    if not isOpscenter:
        time.sleep(70)
        logger.info("Not install opscenter")

    install_agent(s3bucket, opsCenter)

    logger.exe('sudo service opscenterd restart')
    logger.exe('sudo service opscenter-agent restart')
    utils.send_message_to_instaclustr("Node Startup complete", progress="COMPLETE")

if __name__ == "__main__":
    main()




