#!/usr/bin/env python
### Script provided by DataStax.

import os
import subprocess
import shlex
import time
import urllib2
import configureOps
import utils

import logger
import conf

def initial_configurations():
    # Begin configuration this is only run once in Public Packages

    if conf.get_config("AMI", "CurrentStatus") != "Complete!":
        # Configure DataStax variables
        try:
            import ds2_configure
            ds2_configure.run()
        except:
            conf.set_config("AMI", "Error", "Exception seen in %s. Please check ~/datastax_ami/ami.log for more info." % 'ds1_launcher.py')

            logger.exception('ds1_launcher.py')


        # Set ulimit hard limits
        logger.pipe('echo "* soft nofile 32768"', 'sudo tee -a /etc/security/limits.conf')
        logger.pipe('echo "* hard nofile 32768"', 'sudo tee -a /etc/security/limits.conf')
        logger.pipe('echo "root soft nofile 32768"', 'sudo tee -a /etc/security/limits.conf')
        logger.pipe('echo "root hard nofile 32768"', 'sudo tee -a /etc/security/limits.conf')

        # Change permission back to being ubuntu's and cassandra's
        logger.exe('sudo chown -hR ubuntu:ubuntu /home/ubuntu')
        logger.exe('sudo chown -hR tomcat7:tomcat7 /raid0/cassandra', False)
        logger.exe('sudo chown -hR tomcat7:tomcat7 /mnt/cassandra', False)
        logger.exe('sudo chown -hR tomcat7:tomcat7 /etc/cassandra', False)
    else:
        logger.info('Skipping initial configurations.')


def restart_tasks():
    logger.info("AMI Type: " + str(conf.get_config("AMI", "Type")))
    # Create /raid0
    logger.exe('sudo mount -a')
    # Disable swap
    logger.exe('sudo swapoff --all')


def launch_opscenter():
    logger.info('Starting a background process to start OpsCenter after a given delay...')
    subprocess.Popen(shlex.split('sudo -u ubuntu python /home/ubuntu/datastax_ami/ds3_after_init.py &'))

def start_services():
    logger.exe('sudo service tomcat7 stop')
    logger.exe('sudo service cassandra stop')
    logger.exe('sudo service tomcat7 start')
#    logger.exe('sudo service cassandra restart')
    # Actually start the applicationec2-50-19-38-49.compute-1.amazonaws.com
    logger.info('Starting DataStax Community...')
    logger.info('Starting Opscenter Agent')


def run():
    reload(utils)
    logger.info("Running initial configurations")
    initial_configurations()
    utils.send_message_to_instaclustr("Starting", progress="STARTUP")
    restart_tasks()
    utils.send_message_to_instaclustr("Determining who should run OpsCenter", progress="LAUNCH_OPSCENTER")
    start_services()
    time.sleep(40)
    logger.exe('sudo python /home/ubuntu/datastax_ami/configureOps.py', withShell=True)

