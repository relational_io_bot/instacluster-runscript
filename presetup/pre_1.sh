#!/bin/sh
# Now using these: http://uec-images.ubuntu.com/releases/maverick/release/
# Current as of 03/29/2012
### Script provided by DataStax.

if [ ! -f cert-*.pem ];
then
    echo "Cert files not found on machine!"
    exit
fi

sudo apt-get -y install tomcat7

# Download and install repo keys
gpg --keyserver pgp.mit.edu --recv-keys 2B5C1B00
gpg --export --armor 2B5C1B00 | sudo apt-key add -
wget -O - http://installer.datastax.com/downloads/ubuntuarchive.repo_key | sudo apt-key add -
wget -O - http://debian.datastax.com/debian/repo_key | sudo apt-key add -

# Prime for Java installation
sudo echo "sun-java6-bin shared/accepted-sun-dlj-v1-1 boolean true" | sudo debconf-set-selections

# Install Git
sudo apt-get -y update
sudo apt-get -y install git

# Git these files on to the server's home directory
git config --global color.ui auto
git config --global color.diff auto
git config --global color.status auto

git clone -b 2.4 https://relational_io_bot@bitbucket.org/relational_io_bot/instacluster-runscript.git datastax_ami

#git checkout $(head -n 1 presetup/VERSION)
echo "got this far!"
# Install Java

if [ -f /etc/sudoers.tmp ]; then
    exit 1
fi

#lock the sudoers file while we mess with it
sudo su

cp /etc/sudoers /tmp/sudoers.new
#This is currently a very permissive modification to the sudoers file
#needs to be locked down by specifying which options can and connot be
#passed
echo "tomcat7  ALL=(ALL) NOPASSWD: ALL" >> /tmp/sudoers.new
visudo -c -f /tmp/sudoers.new
if [ "$?" -eq "0" ]; then
    cp /tmp/sudoers.new /etc/sudoers
fi
rm /etc/sudoers.tmp

exit
exit

sudo wget https://s3.amazonaws.com/instaclustr-resources/java/jdk-6u31-linux-x64.bin

sudo mkdir -p /opt/java/64
sudo mv jdk-6u31-linux-x64.bin /opt/java/64/
cd /opt/java/64
sudo chmod +x jdk*
sudo ./jdk*
echo "java done!"
# Setup java alternatives
sudo update-alternatives --install "/usr/bin/java" "java" "/opt/java/64/jdk1.6.0_31/bin/java" 1
sudo update-alternatives --set java /opt/java/64/jdk1.6.0_31/bin/java
export JAVA_HOME=/opt/java/64/jdk1.6.0_31

cd /home/ubuntu/datastax_ami
# Begin the actual priming
git pull
sudo python presetup/pre_2.py
sudo chown -R ubuntu:ubuntu . 
rm -rf ~/.bash_history 
history -c
cd /etc/ssl/certs
for old in *.pem; do sudo cp $old `basename $old .pem`.bak; done
cd

echo "Image primed!"


# git pull && rm -rf ~/.bash_history && history -c
