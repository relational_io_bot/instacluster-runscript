from optparse import OptionParser
import shlex
import time
import re
import exceptions
import urllib2
import logger
import conf
import urllib
import json


def commands(response):
    results = []
    for command in response['commands']:
        commandResult = logger.exe(command['command'].encode('utf-8'),withShell=True)
        results.append('errors: ' +commandResult[1] + '\n' if not command['sensitive'] else "Sensitive command executed\n")
    send_message_to_instaclustr("Commands executed", "COMMANDS_OK")
    print "".join(results)
    logger.info("".join(results))


def ok(response):
    logger.info(response['message'])

def error(response):
    logger.error("Error from instaclustr: %s" % response["message"] if response["message"] else "something went wrong")

actions = {"CommandResponse" : commands,
             "OkResponse": ok,
             "ErrorResponse" : error,
             }

def halt_me(message):
    __send_message_to_instaclustr(message, "HALTING", True)

def send_message_to_instaclustr(message, progress):
    __send_message_to_instaclustr(message, progress, False)


def __send_message_to_instaclustr(message, progress, halt_now):
    node_point_url=conf.get_config("Cluster","nodepointurl")
    if not node_point_url:
        use_ec2_userdata(parse_ec2_userdata(get_ec2_data()))
    nodeid = conf.get_config("Node", "Id")
    clustername = conf.get_config("Cluster", "Name")
    clusterowner = conf.get_config("Cluster", "Owner")
    authToken = conf.get_config("Cluster", "AuthToken")
    httpprotocol = "http" if conf.get_config("Cluster", "bootScriptTag") == "dev" else "https"

    firstBoot = not conf.get_config("AMI", "CompletedFirstBoot")

    params = {'instanceid':nodeid, 'clustername':clustername,'clusterowner':clusterowner,'message': message, 'authtoken':authToken, 'logEntry': progress, 'firstboot': firstBoot, 'version': 'V2_4'}

    finished = False
    retryCount = 30
    counter = 0
    logger.info("Sending %s with %s"  % (node_point_url ,urllib.urlencode(params)))
    while not finished and counter < retryCount:
        try:
            if halt_now:
                req = urllib2.Request("%s://%s/nodepoint/iamdead?%s" % (httpprotocol,node_point_url ,urllib.urlencode(params)))
            else:
                req = urllib2.Request("%s://%s/nodepoint?%s" % (httpprotocol,node_point_url ,urllib.urlencode(params)))
            response = urllib2.urlopen(req).read()
            finished = True
        except urllib2.HTTPError, e:
            counter += 1
            time.sleep(1)
            print e
        except IOError, e:
            counter += 1
            time.sleep(1)
            print e
    if counter >= retryCount and halt_now:
        logger.exe("sudo halt now")

    response = json.loads(response)
    logger.info("Sent %s to instaclustr" % message)
    actions[response["type"]](response)

def exit_path(errorMsg, instance_data, append_msg=False):
    if not append_msg:
        # Remove passwords from printing: -p
        p = re.search('(-p\s+)(\S*)', instance_data['userdata'])
        if p:
            instance_data['userdata'] = instance_data['userdata'].replace(p.group(2), '****')

        # Remove passwords from printing: --password
        p = re.search('(--password\s+)(\S*)', instance_data['userdata'])
        if p:
            instance_data['userdata'] = instance_data['userdata'].replace(p.group(2), '****')

        append_msg = " Aborting installation.\n\nPlease verify your settings:\n{0}".format(instance_data['userdata'])

    errorMsg += append_msg

    logger.error(errorMsg)
    conf.set_config("AMI", "Error", errorMsg)

    raise exceptions.AttributeError

def curl_instance_data(url):
    while True:
        try:
            req = urllib2.Request(url)
            return req
        except urllib2.HTTPError:
            logger.info("Failed to grab %s..." % url)
            time.sleep(5)

def get_ec2_data():
    instance_data = {}
    conf.set_config("AMI", "CurrentStatus", "Installation started")

    # Try to get EC2 User Data
    try:
        req = curl_instance_data('http://instance-data/latest/user-data/')
        instance_data['userdata'] = urllib2.urlopen(req).read()

        logger.info("Started with user data set to:")
        logger.info(instance_data['userdata'])
    except Exception, e:
        instance_data['userdata'] = ''
        exit_path("No User Data was set.", instance_data)

    # Find internal IP address for seed list
    req = curl_instance_data('http://instance-data/latest/meta-data/local-ipv4')
    instance_data['internalip'] = urllib2.urlopen(req).read()

    # Find public hostname for JMX
    req = curl_instance_data('http://instance-data/latest/meta-data/public-hostname')
    instance_data['publichostname'] = urllib2.urlopen(req).read()

    # Find launch index for token splitting
    req = curl_instance_data('http://instance-data/latest/meta-data/ami-launch-index')
    instance_data['launchindex'] = int(urllib2.urlopen(req).read())
    conf.set_config("Node", "Index", instance_data['launchindex'])

    req = curl_instance_data('http://instance-data/latest/meta-data/instance-id')
    instance_data['instanceid'] = urllib2.urlopen(req).read()
    conf.set_config("Node", "Id", instance_data['instanceid'])

    # Find reservation-id for cluster-id and jmxpass
    req = curl_instance_data('http://instance-data/latest/meta-data/reservation-id')
    instance_data['reservationid'] = urllib2.urlopen(req).read()
    instance_data['clustername'] = instance_data['reservationid']
    # instance_data['jmx_pass'] = instance_data['reservationid']
    return instance_data

def parse_ec2_userdata(instance_data):
    # Setup parser
    parser = OptionParser()

    # Development options
    # Option that specifies the cluster's name
    parser.add_option("--dev", action="store", type="string", dest="dev")

    # Letters available: ...
    # Option that requires either: Enterprise or Community
    parser.add_option("--version", action="store", type="string", dest="version")
    # Option that specifies how the ring will be divided
    parser.add_option("--totalnodes", action="store", type="int", dest="totalnodes")
    # Option that specifies the cluster's name
    parser.add_option("--clustername", action="store", type="string", dest="clustername")
    # Option that allows for a release version of Enterprise or Community
    parser.add_option("--release", action="store", type="string", dest="release")

    # Option that specifies how the number of Analytics nodes
    parser.add_option("--analyticsnodes", action="store", type="int", dest="analyticsnodes")
    # Option that specifies how the number of Analytics nodes
    parser.add_option("--searchnodes", action="store", type="int", dest="searchnodes")

    # Option that specifies the CassandraFS replication factor
    parser.add_option("--cfsreplicationfactor", action="store", type="int", dest="cfsreplication")

    # Option that specifies the username
    parser.add_option("--username", action="store", type="string", dest="username")
    # Option that specifies the password
    parser.add_option("--password", action="store", type="string", dest="password")

    # Option that specifies the installation of OpsCenter on the first node
    parser.add_option("--opscenter", action="store", type="string", dest="opscenter")
    # Option that specifies an alternative reflector.php
    parser.add_option("--reflector", action="store", type="string", dest="reflector")

    # Unsupported dev options
    # Option that allows for an emailed report of the startup diagnostics
    parser.add_option("--raidonly", action="store_true", dest="raidonly")
    # Option that allows for an emailed report of the startup diagnostics
    parser.add_option("--email", action="store", type="string", dest="email")
    # Option that allows heapsize to be changed
    parser.add_option("--heapsize", action="store", type="string", dest="heapsize")
    # Option that allows an interface port for OpsCenter to be set
    parser.add_option("--opscenterinterface", action="store", type="string", dest="opscenterinterface")

    #instaclustr specific options
    parser.add_option("--clusterowner", action="store", type="string", dest="clusterowner")
    parser.add_option("--authtoken", action="store", type="string", dest="authtoken")
    parser.add_option("--nodePointUrl", action="store", type="string", dest="nodepointurl")
    parser.add_option("--bootScriptTag", action="store", type="string", dest="bootscripttag")


    # Grab provided reflector through provided userdata
    try:
        (options, args) = parser.parse_args(shlex.split(instance_data['userdata']))
    except:
        exit_path("One of the options was not set correctly.", instance_data)

    if not options.analyticsnodes:
        options.analyticsnodes = 0
    if not options.searchnodes:
        options.searchnodes = 0

    if not options.raidonly:
        options.realtimenodes = (options.totalnodes - options.analyticsnodes - options.searchnodes)
        options.seed_indexes = [0, options.realtimenodes, options.realtimenodes + options.analyticsnodes]
    return options

def use_ec2_userdata(options):
    if not options:
        exit_path("EC2 User Data must be set for the DataStax AMI to run.", options)

    if not options.totalnodes:
        exit_path("Missing required --totalnodes (-n) switch.", options)

    if options.totalnodes - options.analyticsnodes - options.searchnodes < 0:
        exit_path("Total nodes assigned > total available nodes", options)

    if options.version:
        if options.version.lower() == "community":
            conf.set_config("AMI", "Type", "Community")
        elif options.version.lower() == "enterprise":
            conf.set_config("AMI", "Type", "Enterprise")
        else:
            exit_path("Invalid --version (-v) argument.", options)
    else:
        exit_path("Missing required --version (-v) switch.", options)

    if conf.get_config("AMI", "Type") == "Community" and (options.cfsreplication or options.analyticsnodes or options.searchnodes):
        exit_path('CFS Replication, Vanilla Nodes, and adding an Analytic Node settings can only be set in DataStax Enterprise installs.', options)

    if options.email:
        logger.info('Setting up diagnostic email using: {0}'.format(options.email))
        conf.set_config("AMI", "Email", options.email)

    if options.clusterowner:
        logger.info('Using cluster owner: {0}'.format(options.clusterowner))
        conf.set_config("Cluster", "Owner", options.clusterowner)

    if options.authtoken:
        logger.info('Using authtoken: {0}'.format(options.authtoken))
        conf.set_config("Cluster", "AuthToken", options.authtoken)

    if options.clustername:
        logger.info('Using cluster name: {0}'.format(options.clustername))
        conf.set_config("Cluster", "Name", options.clustername)

    if options.nodepointurl:
        logger.info('Using nodePointUrl: {0}'.format(options.nodepointurl))
        conf.set_config("Cluster", "nodePointUrl", options.nodepointurl)

    if options.bootscripttag:
        logger.info('Using bootScriptTag: {0}'.format(options.bootscripttag))
        conf.set_config("Cluster", "bootScriptTag", options.bootscripttag)

    logger.info('Using cluster size: {0}'.format(options.totalnodes))
    conf.set_config("Cassandra", "TotalNodes", options.totalnodes)
    logger.info('Using seed indexes: {0}'.format(options.seed_indexes))

    if options.reflector:
        logger.info('Using reflector: {0}'.format(options.reflector))